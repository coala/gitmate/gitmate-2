#!/usr/bin/env bash
set -x -e -o pipefail

# GitMate master deployment script
# This script is meant to be executed inside the master server

COMPOSE_LOCATION="/home/app/gitmate-2"
DOCKER_GROUP_NAME="docker"

# Early checks

which docker

which docker-compose

id -nG | grep -qw "$DOCKER_GROUP_NAME"

# Start deployment

cd $COMPOSE_LOCATION

docker-compose pull

docker-compose up -d
