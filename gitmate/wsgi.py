"""
WSGI config for GitMate project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from dotenv import load_dotenv

from django.core.wsgi import get_wsgi_application

from gitmate.settings import BASE_DIR

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gitmate.settings')

load_dotenv(os.path.join(BASE_DIR, '.env'))

application = get_wsgi_application()
