from IGitt.Interfaces.Actions import MergeRequestActions
from IGitt.Interfaces.MergeRequest import MergeRequest
from IGitt.Interfaces.Commit import Commit
from IGitt.Interfaces.CommitStatus import Status
from IGitt.Interfaces.Actions import PipelineActions
from IGitt.Interfaces import MergeRequestStates

from gitmate_config.models import Repository
from gitmate.utils import lock_igitt_object
from .models import MergeRequestModel

from gitmate_hooks.utils import ResponderRegistrar

def manage_labels( labels,label_to_add,wip_label,pending_review_label,
    approved_label):
    """
    Labels the pull request according to value of label_to_add and
    removes other two labels related to review process
    """
    if(label_to_add=='WIP'):
        labels.add(wip_label)
        labels.discard(pending_review_label)
        labels.discard(approved_label)
    elif(label_to_add=='pending'):
        labels.add(pending_review_label)
        labels.discard(wip_label)
        labels.discard(approved_label)
    else:
        labels.add(approved_label)
        labels.discard(wip_label)
        labels.discard(pending_review_label)
    return labels



@ResponderRegistrar.responder(
    'reviewer',
    MergeRequestActions.OPENED,
    MergeRequestActions.SYNCHRONIZED,
)
def store_head_commit_sha(pr: MergeRequest):
    """
    Stores the list of merge requests along with their heads and updates it on
    synchronizing again.
    """
    MergeRequestModel.objects.update_or_create(
        repo=Repository.from_igitt_repo(pr.repository),
        number=pr.number,
        defaults={'head_sha': pr.head.sha})


@ResponderRegistrar.responder(
    'reviewer',
    MergeRequestActions.OPENED,
    MergeRequestActions.SYNCHRONIZED
)
def check_wip_in_title(
    pr: MergeRequest,
    wip_label: str = 'status/WIP',
    pending_review_label: str = 'status/pending_review',
    approved_label: str = 'status/approved'
):
    """
    Adds work in progress label, if title of the pull request begins with
    "wip". Adds pending review label if none of the labels of the review
    plugin are applied.
    """
    with lock_igitt_object('label mr', pr):
        labels = pr.labels
        # Allows [wip] and WIP:
        if 'wip' in pr.title.lower()[:4]:
            labels=manage_labels(labels,'WIP',wip_label,pending_review_label,
                                approved_label)
        elif(not wip_label in labels and not pending_review_label in labels
        and not approved_label in labels):
            labels=manage_labels(labels,'pending',wip_label,
                                pending_review_label,approved_label)

        pr.labels = labels


@ResponderRegistrar.responder(
    'reviewer',
    MergeRequestActions.OPENED,
    MergeRequestActions.SYNCHRONIZED
)
def check_keywords_in_commit_messages(
    pr: MergeRequest,
    wip_label: str = 'status/WIP',
    pending_review_label: str = 'status/pending_review',
    approved_label: str = 'status/approved',
    enable_fixes_vs_closes: bool = False,
    bug_label: str = 'type/bug',
    no_bug_label_message: str = 'Fixes is used but issue has no bug label',
    bug_label_message: str = 'Closes is used but issue has bug label'
):
    """
    Label the pull request as work in progress and remove pending review/
    approved label if fixes is used but referenced issue doesn't have any
    bug label or Closes is used but referenced issue contains bug label.
    """
    if not enable_fixes_vs_closes:
        return
    with lock_igitt_object('label mr', pr):
        labels = pr.labels
        for issue in pr.will_fix_issues:
            if bug_label not in issue.labels:
                labels=manage_labels(labels,'WIP',wip_label,
                                    pending_review_label,approved_label)
                pr.add_comment(no_bug_label_message)
                break
        for issue in pr.will_close_issues:
            if bug_label in issue.labels:
                labels=manage_labels(labels,'WIP',wip_label,
                                    pending_review_label,approved_label)
                pr.add_comment(bug_label_message)
                break

        pr.labels = labels


@ResponderRegistrar.responder(
    'reviewer', PipelineActions.UPDATED)
def label_based_on_status_of_pipeline_actions(
    commit: Commit,
    wip_label: str = 'status/WIP',
    pending_review_label: str = 'status/pending_review',
    approved_label: str = 'status/approved',
    enable_fixes_vs_closes: bool = False,
    bug_label: str = 'type/bug',
    no_bug_label_message: str = 'Fixes is used but issue has no bug label',
    bug_label_message: str = 'Closes is used but issue has bug label',
    enable_pipeline_actions: bool = False,
    enabled_ack_plugin: bool = False,
):
    """
    Labels the PR as approved when the head commit passes all tests and ack
    plugin is not enabled. Labels the PR as pending review when the head
    commit passes all tests but ack plugin is enabled. Otherwise, labels the
    PR as Work in Progress.
    """
    if not enable_pipeline_actions:
        return
    for db_pr in MergeRequestModel.objects.filter(head_sha=commit.sha):
        pr = db_pr.igitt_pr
        if pr.state in (MergeRequestStates.CLOSED, MergeRequestStates.MERGED):
            continue
        with lock_igitt_object('label mr', pr):
            labels = pr.labels
            if commit.combined_status is Status.SUCCESS:
                if not enabled_ack_plugin:
                    labels=manage_labels(labels,'approved',wip_label,
                                        pending_review_label,approved_label)
                else:
                    labels=manage_labels(labels,'pending',wip_label,
                                        pending_review_label,approved_label)
            else:
                labels=manage_labels(labels,'WIP',wip_label,
                                        pending_review_label,approved_label)
            pr.labels = labels
            #call functions to check if WIP label should be applied
            check_wip_in_title(pr, wip_label,pending_review_label,
                                approved_label)
            check_keywords_in_commit_messages(pr,wip_label,
                                            pending_review_label,approved_label
                                            ,enable_fixes_vs_closes,bug_label,
                                            no_bug_label_message,
                                            bug_label_message)


@ResponderRegistrar.responder(
    'reviewer',
    MergeRequestActions.COMMENTED
)
def ack_plugin_invoked(
    pr: MergeRequest,
    wip_label: str = 'status/WIP',
    pending_review_label: str = 'status/pending_review',
    approved_label: str = 'status/approved',
    enabled_ack_plugin: bool = False,
):
    """
    When ack plugin is invoked (not applicable if ack plugin is disabled):
    1) If any commit unacked, apply WIP label.
    2) If any commit not reviewed yet, apply pending_review label.
    3) Else apply approved
    """
    if not enabled_ack_plugin:
        return
    with lock_igitt_object('label mr', pr):
        labels = pr.labels
        head = pr.head
        if head.combined_status is Status.FAILED:
            labels=manage_labels(labels,'WIP',wip_label,pending_review_label,
                                approved_label)
        elif head.combined_status is Status.PENDING:
            labels=manage_labels(labels,'pending',wip_label,
                                pending_review_label,approved_label)
        else:
            labels=manage_labels(labels,'approved',wip_label,
                                pending_review_label,approved_label)
        pr.labels=labels
