from gitmate.enums import PluginCategory
from gitmate.utils import GitmatePluginConfig


class GitmateReviewerConfig(GitmatePluginConfig):
    name = 'plugins.gitmate_reviewer'
    verbose_name = ('Add WIP/pending_review/approved label based on '
                    'status of Pull Request')
    plugin_category = PluginCategory.PULLS
    description = ('Checks title, commit messages, pipeline '
                    'status and ack status and labels Pull Request accordingly')
