"""
This file contains a sample test case for reviewer to be used as
a reference for writing further tests.
"""
from os import environ
from unittest.mock import MagicMock
from unittest.mock import patch
from unittest.mock import PropertyMock

from rest_framework import status

from gitmate_config.tests.test_base import GitmateTestCase
from IGitt.GitHub.GitHubIssue import GitHubIssue
from IGitt.GitHub.GitHubMergeRequest import GitHubMergeRequest
from IGitt.GitHub.GitHubRepository import GitHubRepository
from IGitt.GitHub.GitHubCommit import GitHubCommit
from IGitt.GitHub.GitHubComment import GitHubComment
from IGitt.GitHub.GitHubUser import GitHubUser
from IGitt.GitHub import GitHubToken
from IGitt.GitLab import GitLabOAuthToken
from IGitt.GitLab.GitLabIssue import GitLabIssue
from IGitt.GitLab.GitLabMergeRequest import GitLabMergeRequest
from IGitt.GitLab.GitLabRepository import GitLabRepository
from IGitt.GitLab.GitLabComment import GitLabComment
from IGitt.GitLab.GitLabCommit import GitLabCommit
from IGitt.GitLab.GitLabUser import GitLabUser
from IGitt.Interfaces.CommitStatus import Status
from IGitt.Interfaces import AccessLevel


class TestGitmateReviewer(GitmateTestCase):

    def setUp(self):
        self.setUpWithPlugin('reviewer')
        self.settings = [{
            'name': 'reviewer',
            'settings': {
                'enable_fixes_vs_closes': True,
                'enable_pipeline_actions': True,
                'enabled_ack_plugin': True
            }
        }]
        self.repo.settings = self.settings
        self.gl_repo.settings = self.settings
        self.github_data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 7},
            'action': 'synchronize'
        }
        self.gitlab_data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'update',
                'iid': 2
            }
        }
        self.gh_commit = GitHubCommit(
            self.repo.token, self.repo.full_name,
            '3f2a3b37a2943299c589004c2a5be132e9440cba')
        self.gl_commit = GitLabCommit(
            self.gl_repo.token, self.gl_repo.full_name,
            'f6d2b7c66372236a090a2a74df2e47f42a54456b')
        self.gh_token = GitHubToken(environ['GITHUB_TEST_TOKEN'])
        self.gl_token = GitLabOAuthToken(environ['GITLAB_TEST_TOKEN'])

    #tests for check_wip_in_title
    @patch.object(GitHubMergeRequest, 'title',
                  new_callable=PropertyMock,
                  return_value='WIP: Pull Request in progress')
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    def test_github_change_label_to_status_wip(self, mocked_labels, *args):
        mocked_labels.return_value.add = MagicMock()
        response = self.simulate_github_webhook_call(
            'pull_request', self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mocked_labels().add.assert_called_with('status/WIP')

    @patch.object(GitLabMergeRequest, 'title',
                  new_callable=PropertyMock,
                  return_value='WIP: Merge Request in progress')
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    def test_gitlab_change_label_to_status_wip(self, mocked_labels, *args):
        mocked_labels.return_value.add = MagicMock()
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', self.gitlab_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mocked_labels().add.assert_called_with('status/WIP')

    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    def test_github_change_label_to_status_pending(self, mocked_labels):
        mocked_labels.return_value.add = MagicMock()
        self.github_data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 7},
            'action': 'opened'
        }
        response = self.simulate_github_webhook_call(
            'pull_request', self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mocked_labels().add.assert_called_with('status/pending_review')

    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    def test_gitlab_change_label_to_status_pending(self, mocked_labels):
        mocked_labels.return_value.add = MagicMock()
        self.gitlab_data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'open',
                'iid': 2
            }
        }
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', self.gitlab_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mocked_labels().add.assert_called_with('status/pending_review')

    #tests for check_keywords_in_commit_messages
    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubCommit, 'message', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'add_comment')
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubIssue, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_fix_issues',
                  new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_close_issues',
                  new_callable=PropertyMock)
    def test_github_disable_checking_keywords(self, m_will_close_issues,
                                              m_will_fix_issues, m_iss_labels,
                                              m_labels, m_add_comment,
                                              m_message, m_commits):
        self.repo.settings = [{
            'name': 'reviewer',
            'settings': {
                'enable_fixes_vs_closes': False
            }
        }]

        m_labels.return_value = set()
        m_commits.return_value = tuple([self.gh_commit])
        m_message.return_value = 'Fixes #1'
        m_will_fix_issues.return_value = {
            GitHubIssue(self.repo.token, self.repo.full_name, 1)
        }
        m_will_close_issues.return_value = set()
        m_iss_labels.return_value = {'type/something'}
        response = self.simulate_github_webhook_call('pull_request',
                                                     self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_add_comment.assert_not_called()


    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubCommit, 'message', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'add_comment')
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubIssue, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_fix_issues',
                  new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_close_issues',
                  new_callable=PropertyMock)
    def test_github_fixes_without_bug_label(self, m_will_close_issues,
                                            m_will_fix_issues, m_iss_labels,
                                            m_labels, m_add_comment, m_message,
                                            m_commits):
        m_labels.return_value = set()
        m_commits.return_value = tuple([self.gh_commit])
        m_message.return_value = 'Fixes #1'
        m_will_fix_issues.return_value = {
            GitHubIssue(self.repo.token, self.repo.full_name, 1)
        }
        m_will_close_issues.return_value = set()
        m_iss_labels.return_value = {'type/something'}
        response = self.simulate_github_webhook_call('pull_request',
                                                     self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called_with({'status/WIP'})
        m_add_comment.assert_called_with('`Fixes` is used but referenced issue'
                                         " doesn't have a bug label, if issue "
                                         'is updated to include the bug label '
                                         'then ask a maintainer to add bug '
                                         'label else use `Closes`.')

    @patch.object(GitLabMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitLabCommit, 'message', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'add_comment')
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    @patch.object(GitLabIssue, 'labels', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'will_fix_issues',
                  new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'will_close_issues',
                  new_callable=PropertyMock)
    def test_gitlab_fixes_without_bug_label(self, m_will_close_issues,
                                            m_will_fix_issues, m_iss_labels,
                                            m_labels, m_add_comment, m_message,
                                            m_commits):
        m_labels.return_value = set()
        m_commits.return_value = tuple([self.gl_commit])
        m_message.return_value = 'Fixes #1'
        m_will_fix_issues.return_value = {
            GitLabIssue(self.gl_repo.token, self.gl_repo.full_name, 1)
        }
        m_will_close_issues.return_value = set()
        m_iss_labels.return_value = {'type/something'}
        response = self.simulate_gitlab_webhook_call('Merge Request Hook',
                                                     self.gitlab_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_add_comment.assert_called_with('`Fixes` is used but referenced issue'
                                         " doesn't have a bug label, if issue "
                                         'is updated to include the bug label '
                                         'then ask a maintainer to add bug '
                                         'label else use `Closes`.')
        m_labels.assert_called_with({'status/WIP'})

    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubCommit, 'message', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'add_comment')
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubIssue, 'labels', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_fix_issues',
                  new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'will_close_issues',
                  new_callable=PropertyMock)
    def test_github_closes_with_bug_label(self, m_will_close_issues,
                                          m_will_fix_issues, m_iss_labels,
                                          m_labels, m_add_comment, m_message,
                                          m_commits):
        m_labels.return_value = set()
        m_commits.return_value = tuple([self.gh_commit])
        m_message.return_value = 'Closes #1'
        m_will_close_issues.return_value = {
            GitHubIssue(self.repo.token, self.repo.full_name, 1)
        }
        m_will_fix_issues.return_value = set()
        m_iss_labels.return_value = {'type/bug'}
        response = self.simulate_github_webhook_call('pull_request',
                                                     self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called_with({'status/WIP'})
        m_add_comment.assert_called_with('`Closes` is used but issue has a bug'
                                         ' label, if issue is updated to '
                                         'remove the bug label then ask a '
                                         'maintainer to remove bug label else '
                                         'use `Fixes`.')

    @patch.object(GitLabMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitLabCommit, 'message', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'add_comment')
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    @patch.object(GitLabIssue, 'labels', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'will_fix_issues',
                  new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'will_close_issues',
                  new_callable=PropertyMock)
    def test_gitlab_closes_with_bug_label(self, m_will_close_issues,
                                          m_will_fix_issues, m_iss_labels,
                                          m_labels, m_add_comment, m_message,
                                          m_commits):
        m_labels.return_value = set()
        m_commits.return_value = tuple([self.gl_commit])
        m_message.return_value = 'Closes #1'
        m_will_close_issues.return_value = {
            GitLabIssue(self.gl_repo.token, self.gl_repo.full_name, 1)
        }
        m_will_fix_issues.return_value = set()
        m_iss_labels.return_value = {'type/bug'}
        response = self.simulate_gitlab_webhook_call('Merge Request Hook',
                                                     self.gitlab_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_add_comment.assert_called_with('`Closes` is used but issue has a bug'
                                         ' label, if issue is updated to '
                                         'remove the bug label then ask a '
                                         'maintainer to remove bug label else '
                                         'use `Fixes`.')
        m_labels.assert_called_with({'status/WIP'})

    #tests for label_based_on_status_of_pipeline_actions
    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP'})
    def test_not_enable_pipeline_actions(self, m_labels):
        self.repo.settings = [{
            'name': 'reviewer',
            'settings': {
                'enable_pipeline_actions': False
            }
        }]
        response = self.simulate_github_webhook_call('pull_request', self.github_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_not_called()

    
    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP'})
    def test_github_closed_pr(self, m_labels):
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 5},
            'action': 'synchronize'
        }
        response = self.simulate_github_webhook_call('pull_request', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'commit': {'sha': '38e969aa17e4024b5b64f45be608eafe69049f23'}
        }
        response = self.simulate_github_webhook_call('status', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_not_called()

    @patch.object(GitLabMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP'})
    def test_gitlab_closed_pr(self, m_labels):
        data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'open',
                'iid': 12
            }
        }
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'project': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
            'commit': {'id': 'b315282d00f2cff0b181fcc277309f2a08163a8e'}
        }
        response = self.simulate_gitlab_webhook_call('Pipeline Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_not_called()

    @patch.object(GitHubCommit, 'combined_status',
                  new_callable=PropertyMock, return_value=Status.SUCCESS)
    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP', 'bug'})
    def test_github_success_on_head_commit_not_enabled_ack_plugin(self, m_labels, m_status):
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 7},
            'action': 'opened'
        }
        response = self.simulate_github_webhook_call('pull_request', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'commit': {'sha': 'f6d2b7c66372236a090a2a74df2e47f42a54456b'}
        }
        response = self.simulate_github_webhook_call('status', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # adds approved_label and removes WIP label
        m_labels.assert_called_with({'status/approved', 'bug'})

    @patch.object(GitLabCommit, 'combined_status',
                  new_callable=PropertyMock, return_value=Status.SUCCESS)
    @patch.object(GitLabMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP', 'bug'})
    def test_gitlab_success_on_head_commit_not_enabled_ack_plugin(self, m_labels, m_status):
        data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'open',
                'iid': 2
            }
        }
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'project': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
            'commit': {'id': '99f484ae167dcfcc35008ba3b5b564443d425ee0'}
        }
        response = self.simulate_gitlab_webhook_call('Pipeline Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # adds approved_label and removes status_labels
        m_labels.assert_called_with({'status/approved', 'bug'})

    @patch.object(GitHubCommit, 'combined_status',
                  new_callable=PropertyMock, return_value=Status.SUCCESS)
    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP', 'bug'})
    def test_github_success_on_head_commit_enabled_ack_plugin(self, m_labels, m_status):
        self.repo.settings = [{
            'name': 'reviewer',
            'settings': {
                'enabled_ack_plugin': True
            }
        }]
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 7},
            'action': 'opened'
        }
        response = self.simulate_github_webhook_call('pull_request', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'commit': {'sha': 'f6d2b7c66372236a090a2a74df2e47f42a54456b'}
        }
        response = self.simulate_github_webhook_call('status', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # adds pending_review and removes WIP label
        m_labels.assert_called_with({'status/pending_review', 'bug'})

    @patch.object(GitLabCommit, 'combined_status',
                  new_callable=PropertyMock, return_value=Status.SUCCESS)
    @patch.object(GitLabMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/WIP', 'bug'})
    def test_gitlab_success_on_head_commit_not_enabled_ack_plugin(self, m_labels, m_status):
        data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'open',
                'iid': 2
            }
        }
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'project': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
            'commit': {'id': '99f484ae167dcfcc35008ba3b5b564443d425ee0'}
        }
        response = self.simulate_gitlab_webhook_call('Pipeline Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # adds pending_review and removes WIP label
        m_labels.assert_called_with({'status/approved', 'bug'})

    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/pending_review'})
    def test_github_failure_on_head_commit(self, m_labels):
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'pull_request': {'number': 110},
            'action': 'synchronize'
        }
        response = self.simulate_github_webhook_call('pull_request', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'repository': {'full_name': environ['GITHUB_TEST_REPO'],
                           'id': 49558751},
            'commit': {'sha': '91aaff6db9564480bf340c618c7ced4623c5be2e'}
        }
        response = self.simulate_github_webhook_call('status', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # removes pending_review/approved label and applied WIP label
        m_labels.assert_called_with({'status/WIP'})

    @patch.object(GitLabMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/pending_review'})
    def test_gitlab_failure_on_head_commit(self, m_labels):
        data = {
            'object_attributes': {
                'target': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
                'action': 'open',
                'iid': 24
            }
        }
        response = self.simulate_gitlab_webhook_call(
            'Merge Request Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # now check the labels on status change
        data = {
            'project': {'path_with_namespace': environ['GITLAB_TEST_REPO']},
            'commit': {'id': '8c31d04e826dc38e6b3e74143f83f9890b89c71c'}
        }
        response = self.simulate_gitlab_webhook_call('Pipeline Hook', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        m_labels.assert_called()
        # adds approved_label and removes status_labels
        m_labels.assert_called_with({'status/WIP'})


    # tests for ack_plugin_invoked
    @patch.object(GitHubMergeRequest, 'labels',
                  new_callable=PropertyMock, return_value={'status/pending_review'})
    def test_ack_plugin_invoked_not_enabled_ack(self,m_labels):
        self.repo.settings = [{
            'name': 'reviewer',
            'settings': {
                'enabled_ack_plugin': False
            }
        }]
        comment_data = {
            'repository': {'full_name': self.repo.full_name, 'id': 49558751},
            'issue': {'number': 7, 'pull_request': {}},
            'comment': {'id': 0},
            'action': 'created'
        }
        response = self.simulate_github_webhook_call('pull_request', self.github_data)
        response = self.simulate_github_webhook_call('issue_comment',comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_not_called()

    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubRepository, 'get_permission_level')
    @patch.object(GitHubCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'body', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'author', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    def test_github_failure_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/approved'}
        m_head.return_value = self.gh_commit
        m_body.return_value = 'unack 3f2a3b3'
        m_author.return_value = GitHubUser(self.gh_token, self.user.username)
        m_sha.return_value = '3f2a3b37a2943299c589004c2a5be132e9440cba'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gh_commit])
        comment_data = {
            'repository': {'full_name': self.repo.full_name, 'id': 49558751},
            'issue': {'number': 7, 'pull_request': {}},
            'comment': {'id': 0},
            'action': 'created'
        }
        response = self.simulate_github_webhook_call('pull_request', self.github_data)
        response = self.simulate_github_webhook_call('issue_comment',comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label changed to wip
        m_labels.assert_called_with('status/WIP')

    @patch.object(GitLabMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitLabRepository, 'get_permission_level')
    @patch.object(GitLabCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'body', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'author', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    def test_gitlab_failure_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/approved'}
        m_head.return_value = self.gl_commit
        m_body.return_value = 'unack f6d2b7c'
        m_author.return_value = GitLabUser(self.gl_token, 0)
        m_sha.return_value = 'f6d2b7c66372236a090a2a74df2e47f42a54456b'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gl_commit])
        comment_data = {
            'project': {'path_with_namespace': self.gl_repo.full_name},
            'object_attributes': {
                'action': 'open',
                'id': 25,
                'iid': 0,
                'noteable_type': 'MergeRequest'
            },
            'merge_request': {'iid': 2}
        }
        response = self.simulate_gitlab_webhook_call('Merge Request Hook',
                                                     self.gitlab_data)
        response = self.simulate_gitlab_webhook_call('Note Hook',
                                                     comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label changed to wip
        m_labels.assert_called_with('status/WIP')

    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubRepository, 'get_permission_level')
    @patch.object(GitHubCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'body', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'author', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    def test_github_pending_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/pending_review'}
        m_head.return_value = self.gh_commit
        m_body.return_value = 'random comment which doesn\'t affect status'
        m_author.return_value = GitHubUser(self.gh_token, self.user.username)
        m_sha.return_value = '3f2a3b37a2943299c589004c2a5be132e9440cba'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gh_commit])
        comment_data = {
            'repository': {'full_name': self.repo.full_name, 'id': 49558751},
            'issue': {'number': 7, 'pull_request': {}},
            'comment': {'id': 0},
            'action': 'created'
        }
        response = self.simulate_github_webhook_call('pull_request', self.github_data)
        response = self.simulate_github_webhook_call('issue_comment',comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label remains pending_review
        m_labels.assert_called_with('status/pending_review')

    @patch.object(GitLabMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitLabRepository, 'get_permission_level')
    @patch.object(GitLabCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'body', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'author', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    def test_gitlab_failure_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/pending_review'}
        m_head.return_value = self.gl_commit
        m_body.return_value = 'random comment which doesn\'t affect status'
        m_author.return_value = GitLabUser(self.gl_token, 0)
        m_sha.return_value = 'f6d2b7c66372236a090a2a74df2e47f42a54456b'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gl_commit])
        comment_data = {
            'project': {'path_with_namespace': self.gl_repo.full_name},
            'object_attributes': {
                'action': 'open',
                'id': 25,
                'iid': 0,
                'noteable_type': 'MergeRequest'
            },
            'merge_request': {'iid': 2}
        }
        response = self.simulate_gitlab_webhook_call('Merge Request Hook',
                                                     self.gitlab_data)
        response = self.simulate_gitlab_webhook_call('Note Hook',
                                                     comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label remains pending_review
        m_labels.assert_called_with('status/WIP')

    @patch.object(GitHubMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitHubRepository, 'get_permission_level')
    @patch.object(GitHubCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'body', new_callable=PropertyMock)
    @patch.object(GitHubComment, 'author', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitHubMergeRequest, 'labels', new_callable=PropertyMock)
    def test_github_success_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/pending_review'}
        m_head.return_value = self.gh_commit
        m_body.return_value = 'ack 3f2a3b3'
        m_author.return_value = GitHubUser(self.gh_token, self.user.username)
        m_sha.return_value = '3f2a3b37a2943299c589004c2a5be132e9440cba'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gh_commit])
        comment_data = {
            'repository': {'full_name': self.repo.full_name, 'id': 49558751},
            'issue': {'number': 7, 'pull_request': {}},
            'comment': {'id': 0},
            'action': 'created'
        }
        response = self.simulate_github_webhook_call('pull_request', self.github_data)
        response = self.simulate_github_webhook_call('issue_comment',comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label changed to approved
        m_labels.assert_called_with('status/approved')    

    @patch.object(GitLabMergeRequest, 'commits', new_callable=PropertyMock)
    @patch.object(GitLabRepository, 'get_permission_level')
    @patch.object(GitLabCommit, 'sha', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'body', new_callable=PropertyMock)
    @patch.object(GitLabComment, 'author', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'head', new_callable=PropertyMock)
    @patch.object(GitLabMergeRequest, 'labels', new_callable=PropertyMock)
    def test_gitlab_failure_on_ack_plugin_invoked(self,m_labels,m_head,m_author,
        m_body,m_sha,m_get_perms,m_commits):
        m_labels.return_value = {'status/pending_review'}
        m_head.return_value = self.gl_commit
        m_body.return_value = 'ack f6d2b7c'
        m_author.return_value = GitLabUser(self.gl_token, 0)
        m_sha.return_value = 'f6d2b7c66372236a090a2a74df2e47f42a54456b'
        m_get_perms.return_value = AccessLevel.CAN_WRITE
        m_commits.return_value = tuple([self.gl_commit])
        comment_data = {
            'project': {'path_with_namespace': self.gl_repo.full_name},
            'object_attributes': {
                'action': 'open',
                'id': 25,
                'iid': 0,
                'noteable_type': 'MergeRequest'
            },
            'merge_request': {'iid': 2}
        }
        response = self.simulate_gitlab_webhook_call('Merge Request Hook',
                                                     self.gitlab_data)
        response = self.simulate_gitlab_webhook_call('Note Hook',
                                                     comment_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        m_labels.assert_called()
        # label changed to approved
        m_labels.assert_called_with('status/WIP')
