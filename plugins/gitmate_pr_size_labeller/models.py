from django.db import models
from django.core.validators import RegexValidator

from gitmate_config.models import SettingsBase


class Settings(SettingsBase):
    size_scheme = models.CharField(
        validators=[RegexValidator('.*({size}).*',
                                   message='Label must contain `{size}`', )],
        default='size/{size}',
        max_length=100,
        help_text='Label scheme to use, use {size} as placeholder')
